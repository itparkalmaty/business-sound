<?php

namespace App\Http\Controllers;

use App\Http\Requests\SchedulePlayerRequest;
use App\Http\Resources\ObjectPlaylistResource;
use App\Http\Resources\PlaylistFilePathResource;
use App\Http\Resources\PlaylistResource;
use App\Http\Resources\PlaylistScheduleResource;
use App\Http\Resources\UserResource;
use App\Models\Company;
use App\Models\Playlist;
use App\Models\PlaylistSchedule;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ObjectController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $playlists = $user->company_playlists();
        $user = Auth::user();
//        dd($user->subscription_end_date);
        $date1 = Carbon::now()->format('Y-m-d');
        $date2 = $user->subscription_end_date;

        $mainPlaylist = [];
//        getting playlists that depend on user companies
//        foreach ($playlists as $playlist){
//
//            foreach ($playlist->playlists as $playlistArray){
//                $mainPlaylist[] = $playlistArray;
//            }
//        }


        // getting only files of playlists that depend on user companies
//        foreach ($playlists as $playlist) {
//
//            foreach ($playlist->playlists as $playlistArray) {
//                foreach ($playlistArray->files as $file) {
//                    $mainPlaylist[] = $file->file;
//                }
//            }
//        }

        return view('object', [
            'companies' => $user->company,
            'date1' => $date1,
            'date2' => $date2
        ]);
//        return response([
//            'playlists' => $mainPlaylist,
//
//        ]);
    }

    public function myObject(Request $request, Company $company)
    {
//        return new UserResource(Auth::user());
//        dd($playlistSchedules->playlistByDay($request->has('day') ? $request->day : Carbon::now()->dayOfWeek));
//        dd($company->playlists());
//        dd(PlaylistScheduleResource::collection($company->schedule($request->has('day') ? $request->day : Carbon::now()->dayOfWeek)));
        $user = Auth::user();
//        dd($user->subscription_end_date);
        $date1 = Carbon::now()->format('Y-m-d');
        $date2 = $user->subscription_end_date;

        return view('my-object', [
            'user' => new UserResource(Auth::user()),
//            'playlists' => Auth::user()->playlistByDay($request->has('day') ? $request->day : Carbon::now()->dayOfWeek)
            'playlists' => PlaylistScheduleResource::collection($company->schedule($request->has('day') ? $request->day : Carbon::now()->dayOfWeek)),
            'company' => $company,
            'date1' => $date1,
            'date2' => $date2,
        ]);
    }

    public function myObjectApi()
    {
        $playlists = Auth::user()->company_playlists();
        dd($playlists);
        foreach ($playlists as $playlist) {

            foreach ($playlist->playlists as $playlistArray) {
                foreach ($playlistArray->files as $file) {
                    $mainPlaylist[] = $file->file;
                }
            }
        }
//        dd($playlists);

    }

//    public function index()
//    {
//        $user = Auth::user();
//        $playlists = PlaylistFilePathResource::collection($user->playlists);
//
//        return view('object', [
////            'playlists' => Playlist::with('files')->find($user)
//            'playlists' => $playlists
//        ]);
//    }
//
//    public function myObject(Request $request)
//    {
////        return new UserResource(Auth::user());
////        dd(Auth::user()->playlistByDay($request->has('day') ? $request->day : Carbon::now()->dayOfWeek));
//
//        return view('my-object', [
//            'user' => new UserResource(Auth::user()),
//            'playlists' => Auth::user()->playlistByDay($request->has('day') ? $request->day : Carbon::now()->dayOfWeek)
//
//        ]);
//    }
//

//    public function myObject(Request $request, Company $company)
//    {
//        return view('my-object', [
//                'company' => $company,
//            ]
//        );
//    }

    public function createSchedule(SchedulePlayerRequest $request)
    {
        $data = $request->all();
        $schedule = PlaylistSchedule::create($data);

        return response($schedule);
    }
}
