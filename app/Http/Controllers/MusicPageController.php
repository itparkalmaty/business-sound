<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Playlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MusicPageController extends Controller
{
    public function index(Request $request)
    {
        $playlists = Playlist::with('files')->get();

        $companies = Auth::user()->company;
        return view('music', compact('playlists', 'companies'));
    }
}
