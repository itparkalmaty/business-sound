<?php

namespace App\Http\Controllers;

use App\Http\Resources\FileResource;
use App\Http\Resources\TuneResource;
use App\Models\File;
use App\Models\Tune;
use Illuminate\Http\Request;

class FileController extends Controller
{
    public function showAll()
    {
        return FileResource::collection(File::get());
    }

    public function showCertain(File $file)
    {
        return new FileResource($file);
    }
}
