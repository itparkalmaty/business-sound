<?php

namespace App\Http\Controllers;

use App\Http\Resources\PlaylistFilePathResource;
use App\Http\Resources\PlaylistResource;
use App\Models\Company;
use App\Models\Playlist;
use App\Models\PlaylistSchedule;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class PlaylistController extends Controller
{
    public function showCertain(Playlist $playlist)
    {
        return $playlist->files->pluck('file');
    }

    public function showCertainUsers(User $user)
    {
        return PlaylistFilePathResource::collection($user->playlists);
    }

    public function parse()
    {
        return Playlist::with('users', 'files')->get();
    }

    public function schedule(Playlist $playlist)
    {
        $schedule = PlaylistSchedule::with('playlists', 'companies')->find($playlist);
        return response($schedule);
    }

    public function addPlaylist(Request $request, Playlist $playlist, Company $company)
    {
        if (!PlaylistSchedule::where('company_id', $company->id)
            ->where('playlist_id',$playlist->id)->where('day',$request->day)->exists()) {
            PlaylistSchedule::create([
               'company_id' => $company->id,
               'playlist_id' => $playlist->id,
               'day' => $request->day
            ]);
            return response(['message' => 'Плейлист добавлен'], 200);
        } else {
            return response(['message' => 'У Вас уже есть данный плейлист'], 400);
        }
    }


    public function deletePlaylist(Request $request, PlaylistSchedule $schedule)
    {
        $schedule->delete();
        return back();
    }

    public function show(Request $request, Playlist $playlist)
    {
        return $playlist->get_only_files();
    }

}
