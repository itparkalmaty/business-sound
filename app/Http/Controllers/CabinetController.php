<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CabinetController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        return view('cabinet', compact('user'));
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        if ($request->has('name')) {
            $user->name = $request->name;
        }

        if ($request->has('email')) {
            $user->email = $request->email;
        }

        if ($request->has('password')) {
            $user->password = Hash::make($request->password);
        }

        if ($request->has('name')) {
            $user->name = $request->name;
        }
//        session()->put('user', $user);
//        session()->save();
        $user->save();
        return redirect()->back();
    }
}
