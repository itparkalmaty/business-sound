<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Testing\Fluent\Concerns\Has;

class UserController extends Controller
{

    public function register(RegisterRequest $request)
    {
        $data = $request->all();
        $data['password'] = Hash::make($request->password);
        $user = User::create($data);

        return response(['data' => $user], 200);
    }

    function login(LoginRequest $request)
    {
        $credentials = $request->validated();
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
//            $token = Auth::user()->createToken(env('APP_NAME'));
//            dd($token);
            return new UserResource(Auth::user());
        }
        return response([
            'message    ' => 'Не правильный логин или пароль',
        ], 400);

    }

    public function logout(Request $request)
    {
//        dd(Auth::user());
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        $request->session()->forget(Auth::user());
        return redirect('/');
    }

//    public function logout(Request $request)
//    {
//        $request->user()->currentAccessToken()->delete();mc
//        return response(['message' => 'Logged out'], 200);
//    }


    public function users()
    {
        return UserResource::collection(User::get());
    }

    public function user(User $user)
    {
        return new UserResource($user);
    }

    public function update(Request $request, User $user)
    {
        if ($request->has('name')) {
            $user->name = $request->name;
        }
        if ($request->has('subscription_start_date')) {
            $user->subscription_start_date = $request->subscription_start_date;
        }
        if ($request->has('subscription_end_date')) {
            $user->subscription_end_date = $request->subscription_end_date;
        }
        if ($request->has(''))
            $user->save();

        return response(['success' => true]);
    }

    public function auth()
    {
        return response($this->login());
    }
}
