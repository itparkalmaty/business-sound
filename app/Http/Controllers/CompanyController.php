<?php

namespace App\Http\Controllers;

use App\Http\Resources\CompanyResource;
use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function showAll()
    {
        return CompanyResource::collection(Company::get());
    }

    public function showCertain(Company $company)
    {
        return new CompanyResource($company);
    }

    public function update(Request $request, Company $company)
    {
        if ($request->has('name')) {
            $company->name = $request->name;
        }
        if ($request->has('country')) {
            $company->country = $request->country;
        }
        if ($request->has('city')) {
            $company->city = $request->city;
        }
        if ($request->has('address')) {
            $company->address = $request->address;
        }
        if ($request->has('subscription_start_date')) {
            $company->subscription_start_date = $request->subscription_start_date;
        }
        if ($request->has('subscription_end_date')) {
            $company->subscription_end_date = $request->subscription_end_date;
        }
        $company->save();

        return response(['success' => true]);
    }

}
