<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Partner;
use App\Models\Sphere;
use Carbon\Carbon;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {

        $contacts = Contact::first();
        $spheres = Sphere::get();
        $partners = Partner::get();
        return view('main',
            compact(
                'contacts',
                'spheres',
                'partners'
            )
        );
    }
}
