<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PartnerRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PartnerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PartnerCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Partner::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/partner');
        CRUD::setEntityNameStrings('Партнер', 'Партнеры');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('name')->label('Название');
        CRUD::column('image')->label('Картинка');
//        $this->crud->addColumn([
//            'name'      => 'image', // The db column name
//            'label'     => 'Логотип партнера', // Table column heading
//            'type'      => 'image',
//            // 'prefix' => 'folder/subfolder/',
//            // image from a different disk (like s3 bucket)
//            // 'disk'   => 'disk-name',
//            // optional width/height if 25px is not ok with you
//            // 'height' => '30px',
//            // 'width'  => '30px',
//        ]);
//        CRUD::column('is_show')->label('Видимость на главной');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PartnerRequest::class);

        CRUD::field('name')->label('Название');
        CRUD::field('image')->label('Картинка');
//        $this->crud->addField([
//            'label'        => "Логотип партнера",
//            'name'         => "image",
//            'filename'     => 0, // set to null if not needed
//            'type'         => 'base64_image',
//            'aspect_ratio' => 1, // set to 0 to allow any aspect ratio
//            'crop'         => false, // set to true to allow cropping, false to disable
//            'src'          => NULL, // null to read straight from DB, otherwise set to model accessor function
//        ]);
//        CRUD::field('is_show')->label('Видимость на главной');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
