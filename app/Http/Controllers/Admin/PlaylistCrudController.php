<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PlaylistRequest;
use App\Models\Tag;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PlaylistCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PlaylistCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Playlist::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/playlist');
        CRUD::setEntityNameStrings('Плейлист', 'Плейлисты');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('name')->label('Название');
//        CRUD::column('files');
        CRUD::column('cover')->type('image')->label('Картинка');
        CRUD::column('description')->label('Описание');
//        CRUD::column('users');
        CRUD::column('total_count')->label('Полный счет');
        CRUD::column('total_duration')->label('Полная длительность');
        CRUD::column('total_size')->label('Полный размер');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PlaylistRequest::class);

        CRUD::field('name')->label('Название');
        CRUD::field('files')->label('Аудиофайлы');
        CRUD::field('cover')->label('Картинка');
        CRUD::field('description')->label('Описание');
        CRUD::field('users')->type('relationship')->model(User::class)->entity('users')->label('Пользователи') ;
//        CRUD::field('tags')->type('relationship')->model(Tag::class)->entity('tags')->label('Теги') ;

        CRUD::field('total_count')->label('Полный счет');
        CRUD::field('total_duration')->label('Полная длительность');
        CRUD::field('total_size')->label('Полный размер');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
