<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\FileRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class FileCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class FileCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\File::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/file');
        CRUD::setEntityNameStrings('Аудиофайл', 'Аудиофайлы');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('artist')->label('Исполнитель');
        CRUD::column('title')->label('Название');
        CRUD::column('duration')->label('Длительность');
//        CRUD::column('tags');
        CRUD::column('bpm')->label('BPM');
        CRUD::column('file')->label('Аудиофайл');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(FileRequest::class);

        CRUD::field('artist')->label('Исполнитель');
        CRUD::field('title')->label('Название');
        CRUD::field('duration')->label('Длительность');
//        CRUD::field('tags')->label('Теги');
        CRUD::field('bpm')->label('BPM');
//        CRUD::field('file');
        $this->crud->addField([
            'name'  => 'file',
            'label' => 'Аудиофайл',
            'type'  => 'browse'
        ]);
//        CRUD::field('playlist_id')->label('Плейлист');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
