<?php

namespace App\Http\Controllers\Api\File;

use App\Http\Controllers\Controller;
use App\Http\Resources\FileReportResource;
use App\Http\Resources\PlaylistReportResource;
use App\Models\File;
use App\Models\Playlist;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    //

    public function update(File $file)
    {
        $file->update([
            'played_times' => $file->played_times + 1,
        ]);

        return response([],200);
    }


    public function updatePlayedTime(File $file,Request $request)
    {
        $file->update([
            'played_time' => $file->played_time+$request->duration
        ]);

        return response([],200);
    }

    public function reportPlaylists()
    {
        return PlaylistReportResource::collection(Playlist::get());
    }

    public function reportFiles()
    {
        return FileReportResource::collection(File::get());
    }
}
