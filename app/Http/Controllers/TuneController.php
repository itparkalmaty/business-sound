<?php

namespace App\Http\Controllers;

use App\Http\Resources\TuneResource;
use App\Models\Tune;
use Illuminate\Http\Request;

class TuneController extends Controller
{
    public function report()
    {
        return TuneResource::collection(Tune::get());
    }
}
