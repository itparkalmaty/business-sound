<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Models\Order;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    public function order(OrderRequest $request)
    {
        $order = Order::create($request->all());

        Mail::send('mail.mail', ['order' => $order], function ($message) {
            $message->to('bsound.kz@gmail.com', 'business-sound.com')->subject('Заявка');
            $message->from('info@business-sound.com','business-sound.com');
        });
        return response(['data' => $order], 200);
    }

    public function orderNotApi(OrderRequest $request)
    {
        $order = Order::create($request->all());

        Mail::send('mail.mail', ['order' => $order], function ($message) {
            $message->to('bsound.kz@gmail.com', 'business-sound.com')->subject('Заявка');
            $message->from('info@business-sound.com','business-sound.com');
        });
        return redirect()->back();
    }


}
