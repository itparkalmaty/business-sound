<?php


namespace App\Http\Services;


use App\Http\Resources\ScheduleResource;
use App\Models\Playlist;
use App\Models\PlaylistSchedule;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class ScheduleService
{
    public function checkSchedules()
    {
        dump(Carbon::now()->dayOfWeek);
        $schedules = PlaylistSchedule::where('day', Carbon::now()->dayOfWeek)->get();
        $this->playDays($schedules);

    }

    public function playDays($schedules)
    {
        foreach ($schedules as $schedule){
            $this->sendPlaylistSchedule($schedule->playlist,$schedule);
        }
    }

    public function sendPlaylistSchedule(Playlist $playlist,PlaylistSchedule $schedule)
    {
        dump( new ScheduleResource($playlist));
        $response = Http::withHeaders([
        ])->post('https://socket.bhub.kz',[
            'event' => "play-playlist-$schedule->company_id",
            'data' => new ScheduleResource($playlist)
        ]);

        dump($response->body());
    }
}
