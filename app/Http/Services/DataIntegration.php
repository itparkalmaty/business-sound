<?php


namespace App\Http\Services;

use App\Models\Company;
use App\Models\File;
use App\Models\Playlist;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class DataIntegration
{

    private $limit_pagination = 0;


    public function integrate($type, $plural_type_id, Model $model)
    {
        $i = 1;
        while ($this->limit_pagination == 0) {
            $response = Http::get("http://my.bubuka.info/api/dst/$type/list?token=" . env('TOKEN_BUBUKA') . "&page=$i");
            $this->parseInsertDatabase($model, $response, $type, $plural_type_id);
            $i++;
        }
    }

    private function parseInsertDatabase(Model $model, $response, $type, $plural_type_id)
    {
        $data = $response->json();

        if (!count($data['result'][$type]))
            $this->limit_pagination = 1;

        foreach ($data['result'][$type] as $item) {
            $item[$plural_type_id] = $item['id'];
            $modelData = $model::where($plural_type_id, $item['id'])->first();
            if (!$modelData)
                $modelData = $model::create($item);
            else
                $modelData->update($data);

            if ($type='playlists') $this->connectMusicData($modelData);

        }
    }

    public function connectMusicData(Playlist $playlist)
    {
//        dump($playlist);
        $response = Http::get("http://my.bubuka.info/api/dst/playlists/get/$playlist->playlist_id?token=" . env('TOKEN_BUBUKA') . "&page=1&limit=500");
        $data = $response->json();
//            dump($data);
        foreach ($data['result']['files'] as $file) $this->saveFile($file, $playlist);
    }

    public function saveFile($data, Playlist $playlist)
    {
        $file = File::where('file_id', $data['id'])->first();
        if (!$file) {
            $data['file_id'] = $data['id'];
            unset($data['id']);
            $file = File::create($data);
        }
        $this->associateMusic($file, $playlist);
    }

    public function associateMusic(File $file,Playlist $playlist)
    {
        if (!$playlist->files->contains($file)) $playlist->files()->save($file);
    }
}
