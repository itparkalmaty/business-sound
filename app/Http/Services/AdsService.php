<?php


namespace App\Http\Services;


use App\Http\Resources\AdResource;
use App\Models\Commercial;
use App\Models\CommercialHour;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class AdsService
{

    public function checkAds()
    {
        $ads = CommercialHour::where('minutes', Carbon::now()->minute)
//            ->where('hours',Carbon::now()->hour)
            ->get();
//        dump($ads);
//        Log::error('played song '.Carbon::now());

        $this->playAds($ads);
    }

    public function playAds($ads)
    {
        foreach ($ads as $ad){
//            dump($ad->commercial);
            $this->sendPlayEvent($ad->commercial,$ad->company_id);
        }
    }

    public function sendPlayEvent(Commercial $ad,$company_id)
    {
        dump($company_id);
        dump($ad);
        $response = Http::withHeaders([
        ])->post('https://socket.bhub.kz',[
            'event' => 'play-ads-'.$company_id,
            'data' => new AdResource($ad)
        ]);

        dump($response->body());
    }




}
