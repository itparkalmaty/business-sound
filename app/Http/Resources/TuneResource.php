<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TuneResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'file_id' => $this->file_id,
            'object_id' => $this->company_id,
            'playback_time' => $this->playback_time,
            'date_time' => $this->date_time
        ];
    }
}
