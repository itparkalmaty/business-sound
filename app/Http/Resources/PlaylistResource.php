<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class PlaylistResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request )
    {
        return [
            'id' => $this->id,
            'file' => $this->file,
            'tags' => $this->tags,
            'cover' => $this->cover,
            'description' => $this->description,
            'total_count' => $this->total_count,
            'total_duration' => $this->total_duration,
            'total_size' => $this->total_size,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'users' => UserResource::collection($this->users),
//            'files' => FileResource::collection($this->files)
        ];
    }
}
