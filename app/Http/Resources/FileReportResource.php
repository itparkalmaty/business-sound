<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FileReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'artist' => $this->artist,
            'title' => $this->title,
            'duration' => $this->duration,
            'tags' => $this->tags,
            'file' => $this->file,
            'playlist_id' => $this->playlist_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'played_time' => $this->played_time,
            'played_times' => $this->played_times
        ];
    }
}
