<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'tag', 'file_id'
    ];

    public function file()
    {
        return $this->belongsTo(File::class);
    }

//    public function playlists()
//    {
//        return $this->hasMany(Playlist::class);
//    }
}
