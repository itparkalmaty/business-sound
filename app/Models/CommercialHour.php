<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommercialHour extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory;

    protected $fillable = [
        'commercial_id', 'day', 'hours', 'minutes'
    ];

    public function commercial()
    {
        return $this->belongsTo(Commercial::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
