<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Commercial extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'title', 'file'
    ];

    public function commercialHours()
    {
        return $this->hasMany(CommercialHour::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
