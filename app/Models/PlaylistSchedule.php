<?php

namespace App\Models;

use App\Http\Resources\PlaylistResource;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlaylistSchedule extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'playlist_id', 'company_id', 'day', 'time'
    ];

    public function playlist()
    {
        return $this->belongsTo(Playlist::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

//    public function playlistByDay($day)
//    {
//        $playDay = PlaylistResource::collection($this->belongsToMany(Playlist::class, 'playlists_users')->whereHas('schedule', function ($query) use ($day) {
//            if (isset($this->company->id)) {
//                $query->where('company_id', $this->company->id)->where('day', $day);
//            }
//        })->get());
//        return $playDay;
//
//    }
}
