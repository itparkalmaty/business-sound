<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name', 'country', 'city', 'address', 'subscription_start_date', 'subscription_end_date', 'licence_url', 'company_id', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function playlists()
    {
        return $this->belongsToMany(Playlist::class,'company_playlist');
    }

    public function schedule($day)
    {
        return $this->hasMany(PlaylistSchedule::class)->where('day',$day)->get();
    }


}
