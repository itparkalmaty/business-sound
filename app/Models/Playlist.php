<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Playlist extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name', 'files', 'cover', 'description', 'total_count', 'total_duration', 'total_size', 'playlist_id', 'user_id'
    ];

    public function files()
    {
        return $this->belongsToMany(File::class, 'a');
    }

    public function get_only_files()
    {
        return $this->belongsToMany(File::class,'a')->get();
    }
    public function totalDuration()
    {
        $files =  $this->hasMany(File::class);
        $totalDuration = 0;
        foreach ($files as $file){
            $totalDuration += $file->played_time;
        }
        return $totalDuration;
    }

    public function totalCount()
    {
        $files =  $this->hasMany(File::class);
        $totalDuration = 0;
        foreach ($files as $file){
            $totalDuration += $file->played_times;
        }
        return $totalDuration;
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'playlists_users');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'tags_playlists');
    }

    public function schedule()
    {
        return $this->hasMany(PlaylistSchedule::class);
    }

}
