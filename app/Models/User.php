<?php

namespace App\Models;

use App\Http\Resources\PlaylistResource;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory, Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'subscription_start_date',
        'subscription_end_date',
        'license_url',
        'playlist_id'
    ];

    public function files()
    {
        return $this->hasManyThrough(File::class, Playlist::class);
    }


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function company()
    {
        return $this->hasMany(Company::class);
    }

    public function company_playlists()
    {
        return $this->hasMany(Company::class)->with('playlists.files')->get();
    }

    public function company_playlists_only_files()
    {
        return $this->hasMany(Company::class)->with('playlists.files')->get()->pluck('playlists');

    }

    public function playlists()
    {
        return $this->belongsToMany(Playlist::class, 'playlists_users');
    }

    public function playlistByDay($day, Company $company)
    {
        return $company->schedule($day);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

}
