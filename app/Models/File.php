<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory;

    protected $fillable = [
        'artist', 'title', 'duration', 'tags', 'bpm', 'file', 'file_id', 'playlist_id',
        'played_time','played_times'
    ];

    protected $casts = [
        'tags'=>'array'
    ];

    public function playlist()
    {
        return $this->belongsTo(Playlist::class);
    }
}
