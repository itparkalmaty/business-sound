const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
 mix.js([
    'resources/js/main.js',
], 'public/js/main.js');

if (mix.inProduction()) {
    mix.version();
}

    mix.styles([
        'resources/css/fonts.css',
        'resources/css/style.css',
        'resources/css/green-audio-player.css',
        'resources/css/portal.css',
    ], 'public/css/style.css');
