<?php

use App\Http\Controllers\PlaylistController;
use App\Http\Services\AdsService;
use App\Http\Services\ScheduleService;
use App\Models\File;
use App\Models\Company;
use App\Models\Playlist;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Http;
use App\Http\Services\DataIntegration;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');


Artisan::command('parse-playlists', function () {
    $integration_service = new DataIntegration();
    $integration_service->integrate('playlists', 'playlist_id', new Playlist());
    $this->info('The command was successful!');
});

Artisan::command('parse-files', function () {
    $integration_service = new DataIntegration();
    $integration_service->integrate('files', 'file_id', new File());

    $this->info('The command was successful!');
});
Artisan::command('parse-companies', function () {
    $integration_service = new DataIntegration();
    $integration_service->integrate('objects', 'company_id', new Company());
    $this->info('The command was successful!');
});

Artisan::command('send-ads', function () {
    $service = new AdsService();
    $service->checkAds();
//    $this->info('the commandos');

});


Artisan::command('send-playlist', function () {
    $schedule = new ScheduleService();
    $schedule->checkSchedules();
});
