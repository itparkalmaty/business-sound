<?php

use App\Http\Controllers\Api\File\ReportController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\PlaylistController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\TuneController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ObjectController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('logout', [UserController::class, 'logout']);
});

//Route::group(['middleware' => ['web']], function () {
//    Route::post('login', [UserController::class, 'login']);
//});

Route::post('register', [UserController::class, 'register']);


Route::get('/dst/users/list', [UserController::class, 'users']);
Route::get('/dst/users/get/{user}', [UserController::class, 'user']);
Route::post('/dst/users/update/{user}', [UserController::class, 'update']);
Route::get('/dst/objects/update/{id}', [UserController::class, 'update']);

Route::get('/dst/files/list', [FileController::class, 'showAll']);
Route::get('/dst/files/get/{file}', [FileController::class, 'showCertain']);

Route::get('/dst/playlists/get/{playlist}', [PlaylistController::class, 'showCertain']);
Route::get('/dst/playlists/getUser/{user}', [PlaylistController::class, 'showCertainUsers']);

Route::get('/dst/playlists/list', [PlaylistController::class, 'parse']);

Route::get('/dst/objects/list', [CompanyController::class, 'showAll']);
Route::get('/dst/objects/get/{company}', [CompanyController::class, 'showCertain']);
Route::get('/dst/objects/statistics', [TuneController::class, 'report']);
Route::get('my-object/get',[ObjectController::class, 'myObjectApi']);
//Route::post('/file/played/{file}',[])

Route::get('/playlist/files/{playlist}',[PlaylistController::class,'show']);
Route::prefix('report')->group(function () {
    Route::post('/{file}', [ReportController::class, 'update']);
    Route::post('/duration/{file}', [ReportController::class, 'updatePlayedTime']);
    Route::get('/playlists/get', [ReportController::class, 'reportPlaylists']);
});

Route::get('/add/{playlist}/{company}', [PlaylistController::class, 'addPlaylist'])->name('add');

Route::post('order', [OrderController::class, 'order'])->name('order');

Route::post('create/schedule', [ObjectController::class, 'createSchedule'])->name('createSchedule');

