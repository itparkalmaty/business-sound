<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('slider', 'SliderCrudController');
    Route::crud('order', 'OrderCrudController');
    Route::crud('file', 'FileCrudController');
    Route::crud('tag', 'TagCrudController');
    Route::crud('playlist', 'PlaylistCrudController');
    Route::crud('company', 'CompanyCrudController');
    Route::crud('user', 'UserCrudController');
    Route::crud('tune', 'TuneCrudController');
    Route::crud('contact', 'ContactCrudController');
    Route::crud('sphere', 'SphereCrudController');
    Route::crud('partner', 'PartnerCrudController');
    Route::crud('commercial', 'CommercialCrudController');
    Route::crud('day', 'DayCrudController');
    Route::crud('playlistschedule', 'PlaylistscheduleCrudController');
    Route::crud('commercialhour', 'CommercialhourCrudController');
}); // this should be the absolute last line of this file