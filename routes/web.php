<?php

use App\Http\Controllers\Controller;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PlaylistController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\PortalController;
use App\Http\Controllers\ObjectController;
use App\Http\Controllers\CabinetController;
use App\Http\Controllers\MusicPageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('locale/{locale}', [Controller::class, 'locale'])->name('locale');

Route::middleware(['set_locale'])->group(function (){
    Route::get('/', [IndexController::class, 'index'])->name('index');
});

Route::post('login', [UserController::class, 'login']);
Route::post('sending', [OrderController::class, 'orderNotApi'])->name('notapiorder');

Route::middleware(['auth'])->group(function (){
    Route::get('/logout', [UserController::class, 'logout'])->name('logout');
    Route::get('/cabinet', [CabinetController::class, 'index'])->name('cabinet');
    Route::post('/user/update', [CabinetController::class, 'update'])->name('updateUser');
    Route::get('/portal', [PortalController::class, 'index'])->name('portal');
    Route::get('/object', [ObjectController::class, 'index'])->name('object');
    Route::get('/music', [MusicPageController::class, 'index'])->name('music');
    Route::get('/my-object/{company}', [ObjectController::class, 'myObject'])->name('my-object');
    Route::get('/delete/{schedule}', [PlaylistController::class, 'deletePlaylist'])->name('delete');
});

