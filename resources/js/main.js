import Swal from 'sweetalert2';
import GreenAudioPlayer from 'green-audio-player';
import {io} from "socket.io-client";
import axios from 'axios';
import Siema from 'siema';

// Гамбургер
const hamburger = document.querySelector('.hamburger');
if (hamburger !== null) {
    const menu = document.querySelector('.header-menu');

    hamburger.addEventListener('click', () => {
        hamburger.classList.toggle('is-active');
        menu.classList.toggle('active');
        if (hamburger.classList.contains('is-active')) {
            document.body.style.overflow = 'hidden';
        } else {
            document.body.style.overflow = '';
        }
    })
}
// Загрузка страницы
const loader = document.querySelector('.loader');

if (loader !== null) {
    window.addEventListener('load', () => {
        loader.style.opacity = '0%';

        setTimeout(() => {
            loader.remove();
        }, 600)
    })
}

// Портал скрипты
const musicCard = document.querySelectorAll('.alboom-card');
const modal = document.querySelector('.modal-playlist');
const modalContent = document.querySelector('.modal-content');
if(musicCard !== null && musicCard !== null && modalContent !== null) {
    musicCard.forEach(item => {
        item.addEventListener('click', () => {
            modal.style.display = 'block'
            document.body.style.overflow = 'hidden'
        });
    });

    modal.addEventListener('click', (e) => {
        const target = e.target;

        if(target === modal) {
            modal.style.display = 'none'
            document.body.style.overflow = ''
        }
    })

    document.addEventListener('keydown', (e) => {
        if((e.code === 'Escape') && (modal.style.display = 'block')) {
            modal.style.display = 'none'
            document.body.style.overflow = ''
        }
    })
}


// Кнопка заявки в лендинге
const btnReq = document.querySelector('.btn-request');
if(btnReq !== null) {
    btnReq.addEventListener('click', () => {
        Swal.fire({
            title: locale === 'ru' ? 'Заполните заявку' : 'Fill up the form',
            icon: 'question',
            confirmButtonText: locale === 'ru' ? `Отправить` : 'Submit',
            html: locale === 'ru' ?
            '<input id="name-request" placeholder="Имя" class="swal2-input">' +
            '<input id="phone" placeholder="Телефон" class="swal2-input">'+
            '<input id="email-request" placeholder="Email" class="swal2-input">' :
            '<input id="name-request" placeholder="Name" class="swal2-input">' +
            '<input id="phone" placeholder="Telephone" class="swal2-input">'+
            '<input id="email-request" placeholder="Email" class="swal2-input">'
            ,
            inputAttributes: {
                autocapitalize: 'off',
                placeholder: 'Имя',
              }, function() {
                  window.location.href = '/cabinet'
              }
        })
        .then((result) => {
            if(result.isConfirmed) {
                let dataObject = {
                    email: document.getElementById('email-request').value,
                    phone: document.getElementById('phone').value,
                    name: document.getElementById('name-request').value
                }
                axios.post('/api/order', {
                    email:dataObject.email,
                    phone:dataObject.phone,
                    name:dataObject.name,
                    header: {
                        "Content-type" : "application/json"
                    }
                })
                .then(response => {
                    Swal.fire({
                        icon: 'success',
                        title: locale === 'ru' ? 'Заявка отправлена!' : 'Application sent',
                        text: locale === 'ru' ? 'Скоро мы с вами свяжемся!' : 'Thank you! We will contact you shortly.'
                      })
                })
                .catch(e => {
                    let errors = [];
                    for(let [key,value] of Object.entries(e.response.data.errors)){
                        errors.push(value)
                    }
                    Swal.fire({
                    icon: 'error',
                    title: locale === 'ru' ? 'Ошибка' : 'Error',
                    text: errors.join('\n')
                    })
                })
            }
        })
    })
}

// Кнопка авторизации
const btnLogin = document.querySelector('.btn-login');
let user = localStorage.getItem('user')


if(btnLogin !== null) {


    btnLogin.addEventListener('click', () => {
        let user = localStorage.getItem('user')
        if(user) {
            window.location.href='/cabinet';
            localStorage.removeItem('user')
            return false;
        }
        Swal.fire({
            title: locale === 'ru' ? 'Авторизация' : 'Authorization',
            icon: 'info',
            confirmButtonText: locale === 'ru' ? `Войти` : 'Log in',
            html:
            locale === 'ru' ? '<form>' + '<input minlength="8" id="email-1" placeholder="E-mail" class="swal2-input">' +
            '<input type="password" id="password-1" placeholder="Пароль" class="swal2-input">' + `</form>` :
            '<form>' + '<input minlength="8" id="email-1" placeholder="E-mail" class="swal2-input">' +
            '<input type="password" id="password-1" placeholder="Password" class="swal2-input">' + `</form>`
            ,
            inputAttributes: {
                autocapitalize: 'off',
                placeholder: locale === 'ru' ? 'Имя' : 'Name',
              },
        })
        .then((result) => {
            if(result.isConfirmed) {
                let dataObject = {
                    email: document.getElementById('email-1').value,
                    password: document.getElementById('password-1').value

                }
                axios.post('/login',{
                    email:dataObject.email,
                    password:dataObject.password,
                    header: {
                        'X-CSRF-TOKEN' : `${document.getElementById('token').value}`
                    }
                }).then (response=>{
                    let logButton = document.getElementById('main-login-button')
                    logButton.innerHTML = response.data.data.name
                    localStorage.setItem('user',JSON.stringify(response.data.data))
                    Swal.fire({
                        icon: 'success',
                        title: locale === 'ru' ? 'Успешно' : 'Success!',
                        text: locale === 'ru' ? 'Вы вошли' : 'You are logged in'
                      })



                }).catch(e=>{
                    let errors = [];
                    for(let [key,value] of Object.entries(e.response.data.errors)){
                        errors.push(value)
                    }
                    Swal.fire({
                        icon: 'error',
                        title: locale === 'ru' ? 'Ошибка' : 'Error',
                        text: errors.join('\n')
                      })
                })

           }

        })
    })
}
// Плеер инициализация
GreenAudioPlayer.init({
    selector: '.track',
    stopOthersOnPlay: true
});


const audioTrack = document.querySelectorAll('.audio-track');

axios.get('api/dst/playlists/getUser/10')
.then((response) => {
    for(let i = 0; i < response.data.data.length; i++) {
        for (let n = 0; n < audioTrack.length; n++) {

            function loadSong() {
                audioTrack[n].src = response.data.data[i].files[songIndex];
            }

            function trackStorage() {
                audioTrack[n].addEventListener('timeupdate', () => {
                    let audioTime =  audioTrack[n].currentTime;
                    if(audioTrack[n].currentTime > 10) {
                        sessionStorage.setItem('time', audioTime);
                        sessionStorage.setItem('track', audioTrack[n].src);
                    // console.log(audioTime);
                    }
                })
            }

            function nextSong() {
                songIndex++;
                if(songIndex > songs.length - 1) {
                    songIndex = 0;
                }
                loadSong(songs[songIndex]);

                // sessionStorage.removeItem('time');
                // sessionStorage.removeItem('track');
                audioTrack[n].play();
                const audioD = document.querySelector('.play-pause-btn__icon');
                audioD.setAttribute('d', 'M0 0h6v24H0zM12 0h6v24h-6z');
                trackStorage();
            }
                audioTrack[n].addEventListener('ended', nextSong);
                audioTrack[n].addEventListener('playing', trackStorage);

            const socket = io('https://socket.bhub.kz/')

            socket.on("connection")


            socket.on('play-ads', (data) => {
                if(audioTrack[n].duration > 0 && !audioTrack[n].paused) {
                    audioTrack[n].pause();
                    console.log(data)
                    let adAudio = new Audio(data.link)
                    adAudio.play()
                    // document.getElementById('message-container').innerHTML += <div>${data.link}</div>
                    adAudio.addEventListener('ended', () => {
                        // if(audioTrack[n].duration > 10) {
                        //     sessionStorage.removeItem('time');
                        //     sessionStorage.removeItem('track');
                        // // }
                        // audioTrack[n].src = thisTrack;
                        // // audioTrack[n].pause();

                        // let thisTrack = sessionStorage.getItem('track')

                        // audioTrack[n].src = thisTrack;
                        // let getTime = sessionStorage.getItem('time');
                        //             audioTrack[n].currentTime = getTime;


                                    audioTrack[n].play();




                    });
                }
            });
        }
        const songs = response.data.data[i].files;
        // console.log(songs)
        let songIndex = 0;
    }
    // const songs = response.data;
    // let songIndex = 0;
    // loadSong(songs[songIndex]);
    // function loadSong() {
    //     audioTrack.src = response.data[songIndex];
    // }
    // function nextSong() {
    //     songIndex++;
    //     if(songIndex > songs.length - 1) {
    //         songIndex = 0;
    //     }
    //     loadSong(songs[songIndex]);
    //     audioTrack.play();
    //     const audioD = document.querySelector('.play-pause-btn__icon');
    //     audioD.setAttribute('d', 'M0 0h6v24H0zM12 0h6v24h-6z');
    // }
    // audioTrack.addEventListener('ended', nextSong);

    // const socket = io('https://socket.bhub.kz/')

    // socket.on("connection")

    // socket.on('play-ads', (data) => {
    //     // audioTrack.pause();
    //     if(audioTrack.currentTime > 0) {
    //         audioTrack.src = data.link;
    //         audioTrack.play()
    //     }
    //     console.log(data)
    //     // audioTrack.src = data.link;
    //     // audioTrack.play()
    //     // document.getElementById('message-container').innerHTML += <div>${data.link}</div>
    //     audioTrack.addEventListener('ended', () => {
    //         audioTrack.pause();
    //         audioTrack.src = 'https://greghub.github.io/green-audio-player/examples/audio/example-1.mp3';
    //         audioTrack.play();
    //     });
    // });

})
.catch((error) => {
    console.log(error);
})

// const songs = [];
// const playlists = [];

// // Отслеживание песен
// let songIndex = 0;

// // Инициализация песен в DOM
// loadSong(songs[songIndex])

// // Обновление песен
// function loadSong(song, playlist) {
//     audio.src = `mp3/${playlist}/${song}.mp3`
// }

// audioTrack.forEach(item => {
//     item.addEventListener('ended', () => {
//         item.src = 'https://greghub.github.io/green-audio-player/examples/audio/example-1.mp3'
//         item.play();
//         console.log('Трек закончен');
//     });

//     const socket = io('https://socket.bhub.kz/')

//     socket.on("connection")


//     socket.on('play-ads', (data) => {
//         item.pause();
//         console.log(data)
//         item.src = data.link;
//         item.play()
//         // document.getElementById('message-container').innerHTML += <div>${data.link}</div>
//         item.addEventListener('ended', () => {
//             item.pause();
//             item.src = 'https://greghub.github.io/green-audio-player/examples/audio/example-1.mp3';
//             item.play();
//         });
//     });

// })

// // Сокет
// if(user) {
//     const socket = io('https://socket.bhub.kz/')

//     socket.on("connection")


//     socket.on('play-ads', (data) => {
//         console.log(data)
//         let audio = new Audio(data.link)
//         audio.play()
//         // document.getElementById('message-container').innerHTML += <div>${data.link}</div>
//     })
// }

// Плейлист

const playBtns = document.querySelectorAll('.playlist-play');
const player = document.querySelector('.audio-track');
const playOffs = document.querySelectorAll('.playlist-off');
const trackName = document.querySelector('#trackname');
const play = document.querySelector('.play-pause-btn__icon');

// if(playOffs != null){
//     playOffs.forEach(playOff=>{
//         playOff.addEventListener('click',function(e){
//             e.preventDefault()

//         })
//     })
// }
let songIndex = 0;

if(playBtns != null){
    playBtns.forEach(playBtn=>{
        playBtn.addEventListener('click',function(){

        })
            axios.get(`/api/playlist/files/${playBtn.getAttribute('data-playlist-id')}`).then(response=>{
                player.src = response.data[0].file;
                trackName.innerHTML = response.data[0].title
                play.addEventListener('click', () => {
                    const socket = io('https://socket.bhub.kz/')

                socket.on("connection")

                console.log(document.getElementById('company-id').value);
                socket.on(`play-ads-${document.getElementById('company-id').value}`, (data) => {
                    console.log(data);
                    let adAudio = new Audio(data.link)
                    player.pause()
                    adAudio.play()
                    adAudio.addEventListener('ended',function(){
                        player.play()
                    })
                })
                })
                playBtn.addEventListener('click',function(){
                player.src = response.data[songIndex].file
                trackName.innerHTML = response.data[songIndex].title
                player.play()
                const audioD = document.querySelector('.play-pause-btn__icon');
                audioD.setAttribute('d', 'M0 0h6v24H0zM12 0h6v24h-6z');
                const socket = io('https://socket.bhub.kz/')

                socket.on("connection")

                console.log(document.getElementById('company-id').value);
                socket.on(`play-ads-${document.getElementById('company-id').value}`, (data) => {
                    console.log(data);
                    let adAudio = new Audio(data.link)
                    player.pause()
                    adAudio.play()
                    adAudio.addEventListener('ended',function(){
                        player.play()
                    })
                })
                player.addEventListener('ended',function(){
                    if(songIndex > response.data.length-1){
                        songIndex = 0
                    } else {
                        songIndex++
                    }

                    player.src = response.data[songIndex].file;
                    trackName.innerHTML = response.data[songIndex].title

                    player.play()
                    audioD.setAttribute('d', 'M0 0h6v24H0zM12 0h6v24h-6z');
            })
                })
        })
    })
}

// Добавление
const textSongs = document.querySelectorAll('#mimodalejemplo');

const modalSongs = document.querySelectorAll('.alboom-card');
modalSongs.forEach(modalSong=>{
    modalSong.addEventListener('click',()=>{
        document.getElementById('#mimodalejemplo').innerHTML = ''
        let playlistInfo = JSON.parse(document.getElementById(`playlist-info-${modalSong.getAttribute('data-playlist-id')}`).value)
        console.log(playlistInfo)
        playlistInfo.files.forEach(file=>{
            document.getElementById('#mimodalejemplo').innerHTML += `
            <div class="trackname">
                                 <h3>${file.artist} - <span>${file.title}</span></h3>
                            </div>
                            `
        })
        document.getElementById('cover-modal').setAttribute('src',playlistInfo.cover)
        document.getElementById('modal-title').innerHTML=playlistInfo.name
        document.getElementById('modal-count').innerHTML = playlistInfo.files.length + ' аудиозаписи'
        document.getElementById('playlist-id').value = playlistInfo.id
    })

})



let addButton = document.getElementById('add-playlist-button')

if(addButton != null){
    addButton.addEventListener('click',()=>{
        let playlistId = document.getElementById('playlist-id').value;
        let companyId = document.getElementById('select-object').value;
        let day = document.getElementById('select-days').value
        axios.get(`/api/add/${playlistId}/${companyId}?day=${day}`)

        .then(response=>Swal.fire({
            icon: 'success',
            title: 'Успешно',
            text: response.data.message,
          }))
        .catch(e=>
            Swal.fire({
            icon: 'error',
            title: 'Ошибка',
            text: e.response.data.message,
          }))
    })
}

// Анимация плюсов работы с нами
const cards = document.querySelectorAll('.plus-card');
if(cards !== null) {

}

if(cards.length > 0) {
    window.addEventListener('scroll', animScroll);
    function animScroll () {
        for (let index = 0; index < cards.length; index++) {
            const card = cards[index],
                  cardHeight = card.offsetHeight,
                  cardOffset = offset(card).top,
                  animStart = 1.5;

                  let animCardPoint = window.innerHeight - cardHeight / animStart;

                  if(cardHeight > window.innerHeight) {
                    window.innerHeight - window.innerHeight / animStart;
                  }

                  if((pageYOffset > cardOffset - animCardPoint) && pageYOffset < (cardOffset + cardHeight)) {
                      card.classList.add('card-active');
                  } else {
                      if (!card.classList.contains('card-active')) {
                        card.classList.remove('card-active');
                      }
                  }
        }
        function offset(el) {
            const rect = el.getBoundingClientRect(),
                  scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
                  scrollTop = window.pageYOffset || document.documentElement.scrollTop;

                  return {top: rect.top + scrollTop, left: rect.left + screenLeft}
        }

    }
}

let locale = document.documentElement.lang;

// Эффект печатающего текста
// const mainText = document.getElementById('text');
// const texts = [
//     locale === 'ru' ? 'Торгового центра' : 'Shopping center',
//     locale === 'ru' ? 'Крупного минимаркета' : 'Large minimarket',
//     locale === 'ru' ? 'Баров и Пабов' : 'Bars and Pubs',
//     locale === 'ru' ? 'Автосалонов' : 'Car Dealerships',
//     locale === 'ru' ? 'Отелей и ресторанов' : 'Hotels and Restaurants',
// ]


// let count = 0;
// let index = 0;
// let currentText = '';
// let letter = '';
// if(mainText !== null) {
//     (function type() {

//         if(count === texts.length) {
//             count = 0;
//         }
//         // delletter = currentText.slice(0, --index);
//         currentText = texts[count];
//         letter = currentText.slice(0, ++index);
//         mainText.textContent = letter;
//         if(letter.length === currentText.length) {
//             count++;
//             index = 0;
//         }
//         setTimeout(type, 300);
//     }())
// }

// Плавный скролл
const smoothLinks = document.querySelectorAll('a[href^="#"]');
for (let smoothLink of smoothLinks) {
    smoothLink.addEventListener('click', function (e) {
        e.preventDefault();
        const id = smoothLink.getAttribute('href');

        document.querySelector(id).scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        });
    });
};


// Анимация надоедливой секции
// const tags = document.querySelectorAll('.text');
// if(tags !== null) {
//     if(tags.length > 0) {
//         window.addEventListener('scroll', animScroll);
//         function animScroll () {
//             for (let index = 0; index < tags.length; index++) {
//                 const tag = tags[index],
//                       tagHeight = tag.offsetHeight,
//                       tagOffset = offset(tag).top,
//                       animStart = 1.5;

//                       let animTagPoint = window.innerHeight - tagHeight / animStart;

//                       if(tagHeight > window.innerHeight) {
//                         window.innerHeight - window.innerHeight / animStart;
//                       }

//                       if((pageYOffset > tagOffset - animTagPoint) && pageYOffset < (tagOffset + tagHeight)) {
//                           tag.classList.add('text-active');
//                       } else {
//                           if (!tag.classList.contains('text-active')) {
//                             tag.classList.remove('text-active');
//                           }
//                       }
//             }
//             function offset(el) {
//                 const rect = el.getBoundingClientRect(),
//                       scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
//                       scrollTop = window.pageYOffset || document.documentElement.scrollTop;

//                       return {top: rect.top + scrollTop, left: rect.left + screenLeft}
//             }

//         }
//     }

// }

const mySiema = new Siema({
    selector: '.siema',
    duration: 600,
    easing: 'ease-out',
    perPage: 1,
    startIndex: 0,
    draggable: false,
    multipleDrag: false,
    threshold: 20,
    loop: true,
    rtl: false,
    onInit: () => {},
    onChange: () => {},
  });

setInterval(() => mySiema.next(), 3000)
