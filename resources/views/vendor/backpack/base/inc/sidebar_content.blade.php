<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('slider') }}'><i class='nav-icon la la-question'></i> Слайдеры</a></li>--}}
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('order') }}'><i class='nav-icon la la-question'></i> Заявки</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('file') }}'><i class='nav-icon la la-question'></i> Аудиофайлы</a></li>
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('tag') }}'><i class='nav-icon la la-question'></i> Теги</a></li>--}}
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('playlist') }}'><i class='nav-icon la la-question'></i> Плейлисты</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('company') }}'><i class='nav-icon la la-question'></i> Объекты пользователи</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('user') }}'><i class='nav-icon la la-question'></i> Пользователи</a></li>
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('tune') }}'><i class='nav-icon la la-question'></i> Tunes</a></li>--}}
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('contact') }}'><i class='nav-icon la la-question'></i> Контакты</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('setting') }}'><i class='nav-icon la la-cog'></i> <span>Текста на главной</span></a></li>
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('sphere') }}'><i class='nav-icon la la-question'></i> Сферы</a></li>--}}
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('partner') }}'><i class='nav-icon la la-question'></i> Партнеры</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('commercial') }}'><i class='nav-icon la la-question'></i> Реклама</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('elfinder') }}"><i class="nav-icon la la-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('day') }}'><i class='nav-icon la la-question'></i> Days</a></li>--}}
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('playlistschedule') }}'><i class='nav-icon la la-question'></i> Расписание плейлистов</a></li>--}}
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('commercialhour') }}'><i class='nav-icon la la-question'></i> Расписание рекламы</a></li>

