<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Кабинет</title>
    <link rel="stylesheet" href="css/style.css">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
          <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
          <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
          <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
          <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
          <link rel="manifest" href="/site.webmanifest">
          <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
          <meta name="msapplication-TileColor" content="#da532c">
          <meta name="theme-color" content="#f23279">
</head>
<body>
<div class="object">
    <div class="container-portal">
        <div class="nav">
            <a href="#" class="logo-portal">
                <img src="img/logo.png" alt="">
            </a>
            <ul>
                <li>
                    <a href="{{ route('index') }}">Главная</a>
                </li>
                <li>
                    <a class='active' href="{{ route('cabinet') }}">Мой кабинет</a>
                </li>
                <li>
                    <a href="#">Помощь</a>
                </li>
                <li>
                    <a href="{{ route('logout') }}">Выход</a>
                </li>
            </ul>
            {{-- <div class="language">
                <img src="img/rus.svg" alt="">
            </div> --}}
        </div>
        <div class="objects">
            <h2>Объекты</h2>
            @foreach($companies as $company)
                {{--                @dd($object)--}}
                <div class="music-card">
                    <a href="{{route('my-object',$company->id)}}">

                        <img src="{{$company}}" alt="">
                        <div class="music-text">
                            <p>{{$company->name}}</p>
                            {{-- <ul class="track">
                            </ul>
                            @if($data1 < $data2)
                                <p>Оплачено</p>
                            @else
                                <p>Не оплачено</p>
                            @endif

                                                   <a href="{{ route('delete', $playlist) }}" class='edit_card'>Редактировать</a> --}}
                        </div>
                        <div class="card-status">
                            <p>Оплачено</p>
                        </div>
                    </a>

                </div>

            @endforeach

        </div>
    </div>
</div>
<script src="js/main.js"></script>
</body>
</html>
