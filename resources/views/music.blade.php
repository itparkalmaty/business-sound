<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Подключить музыку</title>
    <link rel="stylesheet" href="css/style.css">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
          <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
            <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
            <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
            <link rel="manifest" href="/site.webmanifest">
            <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
            <meta name="msapplication-TileColor" content="#da532c">
            <meta name="theme-color" content="#f23279">
</head>
<body>
<div class="music-list">
    <div class="modal-playlist">
        <div class="modal-content">
            <div class="modal-first">
                <img src="img/rap.jpg" alt="" id="cover-modal">
                <h2 id="modal-title">Старый рэп</h2>
                <span id="modal-count">27 аудиозаписей</span>
                <div class="modal-buttons">
                    <input type="hidden" id="playlist-id">
                    {{-- <button class="mini-btn btn-object">выберите объект</button> --}}
                        <select name="select-object" id="select-object">
                            @foreach($companies as $company)
                            <option value="{{ $company->id }}">{{ $company->name }}</option>
                            @endforeach
                        </select>
                    {{-- <button class="mini-btn day-btn">День недели</button> --}}
                        <select name="select-days" id="select-days">
                            <option value="1">Понедельник</option>
                            <option value="2">Вторник</option>
                            <option value="3">Среда</option>
                            <option value="4">Четверг</option>
                            <option value="5">Пятница</option>
                            <option value="6">Суббота</option>
                            <option value="7">Воскресенье</option>
                        </select>
                </div>
                <div class="primary-btn" id="add-playlist-button">+ Добавить</div>
            </div>
            <div class="modal-second">
                    <div id="#mimodalejemplo">

                            <div class="trackname">
                                {{-- <h3>{{ $file->artist }} - <span>{{ $file->title }}</span></h3> --}}
                            </div>

                    </div>

                {{--                <div class="trackname">--}}
                {{--                    <h3>Скриптонит - <span>Чистый</span></h3>--}}
                {{--                </div>--}}
                {{--                <div class="trackname">--}}
                {{--                    <h3>Скриптонит - <span>Чистый</span></h3>--}}
                {{--                </div>--}}
                {{--                <div class="trackname">--}}
                {{--                    <h3>Скриптонит - <span>Чистый</span></h3>--}}
                {{--                </div>--}}
                {{--                <div class="trackname">--}}
                {{--                    <h3>Скриптонит - <span>Чистый</span></h3>--}}
                {{--                </div>--}}
                {{--                <div class="trackname">--}}
                {{--                    <h3>Скриптонит - <span>Чистый</span></h3>--}}
                {{--                </div>--}}
                {{--                <div class="trackname">--}}
                {{--                    <h3>Скриптонит - <span>Чистый</span></h3>--}}
                {{--                </div>--}}
                {{--                <div class="trackname">--}}
                {{--                    <h3>Скриптонит - <span>Чистый</span></h3>--}}
                {{--                </div>--}}
                {{--                <div class="trackname">--}}
                {{--                    <h3>Скриптонит - <span>Чистый</span></h3>--}}
                {{--                </div>--}}
                {{--                <div class="trackname">--}}
                {{--                    <h3>Скриптонит - <span>Чистый</span></h3>--}}
                {{--                </div>--}}
                {{--                <div class="trackname">--}}
                {{--                    <h3>Скриптонит - <span>Чистый</span></h3>--}}
                {{--                </div>--}}
                {{--                <div class="trackname">--}}
                {{--                    <h3>Скриптонит - <span>Чистый</span></h3>--}}
                {{--                </div>--}}
                {{--                <div class="trackname">--}}
                {{--                    <h3>Скриптонит - <span>Чистый</span></h3>--}}
                {{--                </div>--}}
                {{--                <div class="trackname">--}}
                {{--                    <h3>Скриптонит - <span>Чистый</span></h3>--}}
                {{--                </div>--}}
                {{--                <div class="trackname">--}}
                {{--                    <h3>Скриптонит - <span>Чистый</span></h3>--}}
                {{--                </div>--}}
            </div>
        </div>
    </div>
    <div class="container-portal">
        <div class="nav">
            <a href="#" class="logo-portal">
                <img src="img/logo.png" alt="">
            </a>
            <ul>
                <li>
                    <a href="{{ route('index') }}">Главная</a>
                </li>
                <li>
                    <a class='active' href="{{ route('cabinet') }}">Мой кабинет</a>
                </li>
                <li>
                    <a href="#">Помощь</a>
                </li>
                <li>
                    <a href="{{ route('logout') }}">Выход</a>
                </li>
            </ul>
            {{-- <div class="language">
                <img src="img/rus.svg" alt="">
            </div> --}}
        </div>
        <div class="menu">
            <a href="{{ route('cabinet') }}">Личные данные</a>
            <a href="{{ route('object') }}">Мои объекты</a>
            <a class='music-active' href="{{ route('music') }}">Подключить музыку</a>
        </div>
        <div class="music-albooms">
            <div class="portal-tags">
               {{-- <div class="portal-tag">Рок</div>--}}
{{--                <div class="portal-tag">Реп</div>--}}
{{--                <div class="portal-tag">Джаз</div>--}}
{{--                <div class="portal-tag">Классика</div>--}}
{{--                <div class="portal-tag">Спокойная</div>--}}
{{--                <div class="portal-tag">Remix</div>--}}
{{--                <div class="portal-tag">Попса</div>--}}
{{--                <div class="portal-tag">Рок</div>--}}
{{--                <div class="portal-tag">Реп</div>--}}
{{--                <div class="portal-tag">Джаз</div>--}}
{{--                <div class="portal-tag">Классика</div>--}}
{{--                <div class="portal-tag">Спокойная</div>--}}
{{--                <div class="portal-tag">Remix</div>--}}
{{--                <div class="portal-tag">Попса</div> --}}
            </div>
            <div class="alboom">
                @foreach($playlists as $playlist)
                    <div class="alboom-card"
                     data-playlist-id="{{ $playlist->id }}"
                     data-toggle="modal" data-target="#mimodalejemplo_{{ $playlist->id }}">

                     <input type="hidden"
                     id="playlist-info-{{ $playlist->id }}"
                      value="{{ json_encode($playlist) }}">
                     <img src="{{ $playlist->cover }}" alt="">
                        <h2>{{ $playlist->name }}</h2>
                        {{--                <span>27 аудиозаписей</span>--}}
                    </div>
                    {{-- <a href="{{ route('add', $playlist) }}">
                        +
                    </a> --}}
                @endforeach
                {{--            <div class="alboom-card">--}}
                {{--                <img src="img/jaz.jpg" alt="">--}}
                {{--                <h2>Старый рэп</h2>--}}
                {{--                <span>27 аудиозаписей</span>--}}
                {{--            </div>--}}
                {{--            <div class="alboom-card">--}}
                {{--                <img src="img/jaz.jpg" alt="">--}}
                {{--                <h2>Старый рэп</h2>--}}
                {{--                <span>27 аудиозаписей</span>--}}
                {{--            </div>--}}
                {{--            <div class="alboom-card">--}}
                {{--                <img src="img/jaz.jpg" alt="">--}}
                {{--                <h2>Старый рэп</h2>--}}
                {{--                <span>27 аудиозаписей</span>--}}
                {{--            </div>--}}
                {{--            <div class="alboom-card">--}}
                {{--                <img src="img/jaz.jpg" alt="">--}}
                {{--                <h2>Старый рэп</h2>--}}
                {{--                <span>27 аудиозаписей</span>--}}
                {{--            </div>--}}
                {{--            <div class="alboom-card">--}}
                {{--                <img src="img/jaz.jpg" alt="">--}}
                {{--                <h2>Старый рэп</h2>--}}
                {{--                <span>27 аудиозаписей</span>--}}
                {{--            </div>--}}
                {{--            <div class="alboom-card">--}}
                {{--                <img src="img/jaz.jpg" alt="">--}}
                {{--                <h2>Старый рэп</h2>--}}
                {{--                <span>27 аудиозаписей</span>--}}
                {{--            </div>--}}
                {{--            <div class="alboom-card">--}}
                {{--                <img src="img/jaz.jpg" alt="">--}}
                {{--                <h2>Старый рэп</h2>--}}
                {{--                <span>27 аудиозаписей</span>--}}
                {{--            </div>--}}
                {{--            <div class="alboom-card">--}}
                {{--                <img src="img/jaz.jpg" alt="">--}}
                {{--                <h2>Старый рэп</h2>--}}
                {{--                <span>27 аудиозаписей</span>--}}
                {{--            </div>--}}
                {{--            <div class="alboom-card">--}}
                {{--                <img src="img/jaz.jpg" alt="">--}}
                {{--                <h2>Старый рэп</h2>--}}
                {{--                <span>27 аудиозаписей</span>--}}
                {{--            </div>--}}
                {{--            <div class="alboom-card">--}}
                {{--                <img src="img/jaz.jpg" alt="">--}}
                {{--                <h2>Старый рэп</h2>--}}
                {{--                <span>27 аудиозаписей</span>--}}
                {{--            </div>--}}
                {{--            <div class="alboom-card">--}}
                {{--                <img src="img/jaz.jpg" alt="">--}}
                {{--                <h2>Старый рэп</h2>--}}
                {{--                <span>27 аудиозаписей</span>--}}
                {{--            </div>--}}
                {{--            <div class="alboom-card">--}}
                {{--                <img src="img/jaz.jpg" alt="">--}}
                {{--                <h2>Старый рэп</h2>--}}
                {{--                <span>27 аудиозаписей</span>--}}
                {{--            </div>--}}
                {{--            <div class="alboom-card">--}}
                {{--                <img src="img/jaz.jpg" alt="">--}}
                {{--                <h2>Старый рэп</h2>--}}
                {{--                <span>27 аудиозаписей</span>--}}
                {{--            </div>--}}
                {{--            <div class="alboom-card">--}}
                {{--                <img src="img/jaz.jpg" alt="">--}}
                {{--                <h2>Старый рэп</h2>--}}
                {{--                <span>27 аудиозаписей</span>--}}
                {{--            </div>--}}
            </div>
        </div>
    </div>
    <script src="js/main.js"></script>
</body>
</html>
