<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Кабинет</title>
    <link rel="stylesheet" href="css/style.css">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
          <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
          <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
          <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
          <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
          <link rel="manifest" href="/site.webmanifest">
          <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
          <meta name="msapplication-TileColor" content="#da532c">
          <meta name="theme-color" content="#f23279">
</head>
<body>
<div class="cabinet">
    <div class="container-portal">
        <div class="nav">
            <a href="#" class="logo-portal">
                <img src="img/logo.png" alt="">
            </a>
            <ul>
                <li>
                    <a href="{{ route('index') }}">Главная</a>
                </li>
                <li>
                    <a class='active' href="{{ route('cabinet') }}">Мой кабинет</a>
                </li>
                <li>
                    <a href="#">Помощь</a>
                </li>
                <li>
                    <a href="{{ route('logout') }}">Выход</a>
                </li>
            </ul>
            {{-- <div class="language">
                <img src="img/rus.svg" alt="">
            </div> --}}
        </div>
        <div class="menu">
            <a class='active-btn' href="{{ route('cabinet') }}">Личные данные</a>
            <a href="{{ route('object') }}">Мои объекты</a>
            <a href="{{ route('music') }}">Подключить музыку</a>
        </div>
        <div class="cabinet-form">
            <form action="{{ route('updateUser') }}" method="post">
                @csrf
                <div>
                    <label>Имя</label>
                    <input type="text" name="name" id="name" value="{{ $user->name }}">
                </div>
                <div>
                    <label>Почта</label>
                    <input type="email" name="email" id="email" value="{{ $user->email }}">
                </div>
                <div>
                    <label>Логин</label>
                    <input type="text" name="login" id="email" value="{{ $user->email }}">
                </div>
                <div>
                    <label>Пароль</label>
                    <input type="password" name="password" id="password" value="">
                </div>
                <button class="btn-portal" type="submit">Сохранить изменения</button>
            </form>
            <img src="img/rock.jpg" alt="">

        </div>
    </div>
</body>
</html>

