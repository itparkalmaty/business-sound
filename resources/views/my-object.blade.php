<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Кабинет</title>
    <link rel="stylesheet" href="/css/style.css">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
          <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
          <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
          <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
          <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
          <link rel="manifest" href="/site.webmanifest">
          <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
          <meta name="msapplication-TileColor" content="#da532c">
          <meta name="theme-color" content="#f23279">
</head>
<body>
<div class="my-object">
    <div class="container-portal">
        <div class="nav">
            <a href="#" class="logo-portal">
                <img src="/img/logo.png" alt="">
            </a>
            <ul>
                <li>
                    <a href="{{ route('index') }}">Главная</a>
                </li>
                <li>
                    <a class='active' href="{{ route('cabinet') }}">Мой кабинет</a>
                </li>
                <li>
                    <a href="#">Помощь</a>
                </li>
                <li>
                    <a href="{{ route('logout') }}">Выход</a>
                </li>
            </ul>
            {{-- <div class="language">
                <img src="img/rus.svg" alt="">
            </div> --}}
        </div>
        <div class="my-object-wrapper">
            <div class="music-card playlist-card">
                @if($date1 < $date2)
                    <img src="img/pull.jpg" alt="">
                    <div class="music-text">
                        <p>{!! $company->name !!}, {!! $company->address !!}</p>
                        <!-- <div class="track">
                            <img src="img/icons/play.svg" alt="">
                            <span>Bambola - Betta Lemme</span>
                        </div> -->
                        <h4 id="trackname">Название исполнителя - трек</h4>
                        <div class="track">
                            <input type="hidden" id="company-id" value="{{ $company->id }}">
                            <audio class="audio-track" src="mp3/juicy.mp3"></audio>
                        </div>
                        {{-- <p class='edit_card'>Редактировать</p> --}}
                    </div>

                    <div class="card-status">
                        <p>Оплачено</p>
                    </div>
                @else
                    <div class="card-status">
                        <p>Не оплачено</p>
                    </div>
                @endif

            </div>
            <div class="days">
                <a href='/my-object/{{$company->id}}?day=1' class="btn-day">ПН</a>
                <a href='/my-object/{{$company->id}}?day=2' class="btn-day">ВТ</a>
                <a href='/my-object/{{$company->id}}?day=3' class="btn-day">СР</a>
                <a href='/my-object/{{$company->id}}?day=4' class="btn-day">ЧТ</a>
                <a href='/my-object/{{$company->id}}?day=5' class="btn-day">ПТ</a>
                <a href='/my-object/{{$company->id}}?day=6' class="btn-day">СБ</a>
                <a href='/my-object/{{$company->id}}?day=7' class="btn-day">ВС</a>
            </div>
            <div class="playlists">
                    @if(isset($playlists) && $date1 < $date2)
                        @foreach($playlists as $playlist)
                            {{--                    <input type="hidden" id="playlist-{{$loop->index}}" value="{{json_encode($playlist)}}">--}}
                            <div class="playlist">
                                <img src="{{ $playlist->playlist->cover }}" alt="">
                                <h2>{{ $playlist->playlist->name }}</h2>
                                {{--                            <span>27 аудиозаписей</span>--}}
                                <div class="playlist-func">
                                    <a class='playlist-play' href="javascript:void(0)"
                                       data-playlist-id="{{$playlist->playlist->id}}">Включить</a>
                                    <a class='playlist-off' href="/delete/{{ $playlist->id }}">Удалить</a>
                                </div>
                            </div>
                        @endforeach
                    @endif
                {{--                <div class="playlist">--}}
                {{--                    <img src="img/rap.jpg" alt="">--}}
                {{--                    <h2>Старый рэп</h2>--}}
                {{--                    <span>27 аудиозаписей</span>--}}
                {{--                    <div class="playlist-func">--}}
                {{--                        <a class='playlist-play' href="#">Включить</a>--}}
                {{--                        <a class='playlist-off' href="#">Выключить</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--                <div class="playlist">--}}
                {{--                    <img src="img/rap.jpg" alt="">--}}
                {{--                    <h2>Старый рэп</h2>--}}
                {{--                    <span>27 аудиозаписей</span>--}}
                {{--                    <div class="playlist-func">--}}
                {{--                        <a class='playlist-play' href="#">Включить</a>--}}
                {{--                        <a class='playlist-off' href="#">Выключить</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--                <div class="playlist">--}}
                {{--                    <img src="img/rap.jpg" alt="">--}}
                {{--                    <h2>Старый рэп</h2>--}}
                {{--                    <span>27 аудиозаписей</span>--}}
                {{--                    <div class="playlist-func">--}}
                {{--                        <a class='playlist-play' href="#">Включить</a>--}}
                {{--                        <a class='playlist-off' href="#">Выключить</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--                <div class="playlist">--}}
                {{--                    <img src="img/rap.jpg" alt="">--}}
                {{--                    <h2>Старый рэп</h2>--}}
                {{--                    <span>27 аудиозаписей</span>--}}
                {{--                    <div class="playlist-func">--}}
                {{--                        <a class='playlist-play' href="#">Включить</a>--}}
                {{--                        <a class='playlist-off' href="#">Выключить</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--                <div class="playlist">--}}
                {{--                    <img src="img/rap.jpg" alt="">--}}
                {{--                    <h2>Старый рэп</h2>--}}
                {{--                    <span>27 аудиозаписей</span>--}}
                {{--                    <div class="playlist-func">--}}
                {{--                        <a class='playlist-play' href="#">Включить</a>--}}
                {{--                        <a class='playlist-off' href="#">Выключить</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--                <div class="playlist">--}}
                {{--                    <img src="img/rap.jpg" alt="">--}}
                {{--                    <h2>Старый рэп</h2>--}}
                {{--                    <span>27 аудиозаписей</span>--}}
                {{--                    <div class="playlist-func">--}}
                {{--                        <a class='playlist-play' href="#">Включить</a>--}}
                {{--                        <a class='playlist-off' href="#">Выключить</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>
        </div>
    </div>
</div>
<script src="/js/main.js"></script>
<script>
    //   Выделение активных ссылок
const currentLocation = location.href;
const menuItem = document.querySelectorAll('.btn-day');
const btnLength = menuItem.length;
for (let i = 0; i < btnLength; i++) {
    if(menuItem[i].href === currentLocation) {
        menuItem[i].classList.add('btn_active');
    }
}
</script>
</body>
</html>
