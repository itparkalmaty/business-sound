<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class IndexSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'RealAdmin2',
            'email' => '6th@button.com',
            'password' => Hash::make('17th@text.com'),
            'is_admin' => 1,
        ]);
//        DB::table('settings')->insert([
//            'key' => '20th_title',
//            'name' => 'Второй заголовок третьего блока',
//            'description' => 'Второй заголовок в третьем блоке',
//            'value' => 'Почему мы?',
//            'field' => '{"name":"value","label":"Value","type":"text"}',
//            'active' => 1,
//        ]);
//        DB::table('settings')->insert([
//            'key' => '23th_text',
//            'name' => 'Второй заголовок третьего блока',
//            'description' => 'Описание в третьем блоке',
//            'value' => 'Сложно представить заведение, в котором не играет музыка. Она есть везде - в ресторанах, барах, кафе, магазинах, бутиках, торговых центрах, спортзалах, и так далее. Музыка должна подходить под атмосферу заведения, Ведь правильное музыкальное оформление увеличивает количество продаж! Используя музыку, можно управлять эмоциональным состоянием посетителя, таким образом что 70% примут положительное решение о покупке! А ещё, музыка должна быть легальной!',
//            'field' => '{"name":"value","label":"Value","type":"textarea"}',
//            'active' => 1,
//        ]);


    }
}
